package tests;

import driver.DriverProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.BingPage;

public class BingTests {
  private static final String SEARCH_TEXT = "weather in Lviv";
  private static final String CITY_NAME = "Lviv, Ukraine";
  private static final String CHAT_ALERT =
      "Chat mode is only available when you have access to the new Bing.";
  private static final String SETTINGS_PAGE_TITLE = "Settings";

  private BingPage steps;

  @BeforeMethod
  public void prepareWebDriver() {
    DriverProvider.getInstance();
    steps = new BingPage();
  }

  @AfterMethod
  public void closeWebDriver() {
    DriverProvider.quit();
  }

  @Test
  public void searchTest() {
    steps.openBingPage().searchInformation(SEARCH_TEXT).verifyWeatherTitleIsDisplayed(CITY_NAME);
  }

  @Test
  public void chatTest() {
    steps.openBingPage().openChatPage().verifyChatAlertText(CHAT_ALERT);
  }

  @Test
  public void settingsTest() {
    steps.openBingPage().openSettingsPage().verifySettingsTitle(SETTINGS_PAGE_TITLE);
  }
}
