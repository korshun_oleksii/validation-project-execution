package driver;

import java.time.Duration;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Waiter {
  public static final int DEFAULT_WAIT_TIME = 30;

  public static WebElement waitForVisibilityOfElement(WebElement element, int seconds) {
    return new WebDriverWait(DriverProvider.getInstance(), Duration.ofSeconds(seconds))
        .ignoring(StaleElementReferenceException.class)
        .ignoring(NoSuchElementException.class)
        .pollingEvery(Duration.ofSeconds(1))
        .until(ExpectedConditions.visibilityOf(element));
  }
}
