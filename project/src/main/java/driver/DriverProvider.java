package driver;

import static java.util.Objects.isNull;

import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

@Log4j2
public class DriverProvider {
  private static final ThreadLocal<WebDriver> driver = new ThreadLocal<>();

  private DriverProvider() {}

  public static WebDriver getInstance() {
    if (isNull(driver.get())) {
      log.info("Start the thread with ID " + Thread.currentThread().getId());
      WebDriverManager.chromedriver().setup();
      driver.set(new ChromeDriver());
      driver.get().manage().deleteAllCookies();
      driver.get().manage().window().maximize();
    }
    return driver.get();
  }

  public static void quit() {
    log.info("Remove The thread with ID " + Thread.currentThread().getId());
    driver.get().quit();
    driver.remove();
  }
}
