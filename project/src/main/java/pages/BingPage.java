package pages;

import static driver.Waiter.DEFAULT_WAIT_TIME;

import driver.DriverProvider;
import driver.Waiter;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

@Log4j2
public class BingPage extends AbstractPage {
  private static final String BING_PAGE_URL = "https://www.bing.com/";

  @FindBy(name = "q")
  private WebElement searchField;

  @FindBy(id = "search_icon")
  private WebElement searchButton;

  @FindBy(xpath = "//div[text()='Chat']")
  private WebElement chatButton;

  @FindBy(xpath = "//a[@id='id_sc']")
  private WebElement settingsAndQuickLinksButton;

  @FindBy(id = "hbsettings")
  private WebElement settingsButton;

  @FindBy(xpath = "//div[text()='More']")
  private WebElement moreButton;

  public BingPage openBingPage() {
    log.info("Init web driver and open the {} url", BING_PAGE_URL);
    DriverProvider.getInstance().get(BING_PAGE_URL);
    return this;
  }

  public SearchResultsPage searchInformation(String text) {
    log.info("Search the information about {}", text);
    Waiter.waitForVisibilityOfElement(searchField, DEFAULT_WAIT_TIME).sendKeys(text);
    Waiter.waitForVisibilityOfElement(searchButton, DEFAULT_WAIT_TIME).click();
    return new SearchResultsPage();
  }

  public SearchResultsPage openChatPage() {
    log.info("Open the chat page");
    Waiter.waitForVisibilityOfElement(chatButton, DEFAULT_WAIT_TIME).click();
    return new SearchResultsPage();
  }

  public SettingsPage openSettingsPage() {
    log.info("Open the settings page");
    Waiter.waitForVisibilityOfElement(settingsAndQuickLinksButton, DEFAULT_WAIT_TIME).click();
    Waiter.waitForVisibilityOfElement(settingsButton, DEFAULT_WAIT_TIME).click();
    Waiter.waitForVisibilityOfElement(moreButton, DEFAULT_WAIT_TIME).click();
    return new SettingsPage();
  }
}
