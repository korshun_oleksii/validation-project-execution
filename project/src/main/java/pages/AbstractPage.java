package pages;

import driver.DriverProvider;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class AbstractPage {
  public AbstractPage() {
    PageFactory.initElements(DriverProvider.getInstance(), this);
  }
}
