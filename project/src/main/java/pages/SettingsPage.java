package pages;

import static driver.Waiter.DEFAULT_WAIT_TIME;

import driver.Waiter;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

@Log4j2
public class SettingsPage extends AbstractPage {
  @FindBy(xpath = "//h2[@class='pb-title-nav-strip__title']")
  private WebElement settingsTitle;

  public void verifySettingsTitle(String titleName) {
    log.info("Verify the settings title name");
    Waiter.waitForVisibilityOfElement(settingsTitle, DEFAULT_WAIT_TIME);
    Assert.assertEquals(settingsTitle.getText(), titleName);
  }
}
