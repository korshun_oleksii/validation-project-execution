package pages;

import static driver.Waiter.DEFAULT_WAIT_TIME;

import driver.Waiter;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

@Log4j2
public class SearchResultsPage extends AbstractPage {
  @FindBy(xpath = "//span[@class='wtr_foreGround']")
  private WebElement cityName;

  @FindBy(xpath = "//span[@class='no_outline']")
  private WebElement chatAlert;

  public void verifyWeatherTitleIsDisplayed(String cityNameText) {
    log.info("Verify the weather info is displayed correctly");
    Assert.assertEquals(
        Waiter.waitForVisibilityOfElement(cityName, DEFAULT_WAIT_TIME).getText(), cityNameText);
  }

  public void verifyChatAlertText(String alertTitle) {
    log.info("Verify the chat alert text");
    Assert.assertEquals(
        Waiter.waitForVisibilityOfElement(chatAlert, DEFAULT_WAIT_TIME).getText(), alertTitle);
  }
}
